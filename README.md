# Gallifreyan Clock

This is an app showing the current time in Gallifreyan

Usage
=====

```
$ pip install -r requirements.txt
$ python __main__.py
```


[![Watch the video](https://i.imgur.com/kpcB3lv.gif)](https://i.imgur.com/kpcB3lv.gif)

<video src="https://i.imgur.com/CigJrzn.mp4" width="320" height="200" controls preload></video>
